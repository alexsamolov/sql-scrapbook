# SQL Scrapbook #

This repo contains scrapbook for various tests and examples in SQL.

### Examples ###

* pagelimit/ - example of query which finds a value in the table and select it, including in addition the whole page where the value is found (smth like LIMIT N OFFSET M).

### Who do I talk to? ###

I hope you'll find this useful. In case of any questions please do not hesitate to contact me: Alexander Samolov (alexsamolov@gmail.com)