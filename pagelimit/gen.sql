DECLARE @J, @G1, @G2;
SET @G1 = DATE('2014-01-01', '2014-08-01', 0); /* Random date */
SET @G2 = STRING(3, 10, 3); /* Random string generator
                             3 words between 10 and 20 characters */
SET @J = 0;
WHILE @J < 200
BEGIN
    INSERT INTO items(dt, value) VALUES ('@G1', '@G2');
    SET @J = @J + 1;
END
