DROP TABLE IF EXISTS items;

CREATE TABLE items (
    id  serial PRIMARY KEY,
    dt  timestamp not null default current_timestamp,
    value varchar(255)
);

CREATE INDEX idx_items_dt_desc ON items (dt DESC);
CREATE INDEX idx_items_value ON items (value varchar_pattern_ops);

INSERT INTO ITEMS(DT, VALUE) VALUES (DATE '2014-04-22', 'Hello there!');


create view items_ranked as SELECT id, dt, value, row_number() over (order by dt desc) rank FROM items;