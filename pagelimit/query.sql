
---------

with i as (
    SELECT id, dt, value, row_number() over (order by dt desc) rank FROM items
)
select out.*, out.rank = w.rank found from 
i out, (
select rank, rank / 10 low_page, 1 + rank / 10 high_page
from i
  where value like '%j %' limit all)
w
where out.rank between low_page*10 and high_page*10 - 1

-----------
with i as (
    SELECT id, dt, value, row_number() over (order by dt desc) rank FROM items
)
select * from 
i out
where exists (
select rank
from i
  where value like '%j %' and out.rank between (rank / 10)*10 and (1 + rank / 10)*10 - 1) ;
